﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tunniplaanv2
{
    public class Ajad
    {
        
        public string t1 { get; set; }
        public string t2 { get; set; }
        public string t3 { get; set; }
        public string t4 { get; set; }
        public string t5 { get; set; }
        public string t6 { get; set; }
        public string t7 { get; set; }
        public string t8 { get; set; }
        public string t9 { get; set; }
        public string t10 { get; set; }
        public string t11 { get; set; }
        public string t12 { get; set; }
        public string t13 { get; set; }
        public string t14 { get; set; }
        public string soomine { get; set; }
    }

    public class Tund
    {
        public string tund { get; set; }
        public string algus { get; set; }
        public string lopp { get; set; }
        public string aine { get; set; }
        public string grupp { get; set; }
        public string opetaja { get; set; }
        public string ruum { get; set; }
    }


    public class Tunnid
    {
        public Tunnid()
        {
            nadal.Add(esmaspaev);
            nadal.Add(teisipaev);
            nadal.Add(kolmapaev);
            nadal.Add(neljapaev);
            nadal.Add(reede);
        }

        private List<Tund> esmaspaev = new List<Tund>();
        private List<Tund> teisipaev = new List<Tund>();
        private List<Tund> kolmapaev = new List<Tund>();
        private List<Tund> neljapaev = new List<Tund>();
        private List<Tund> reede = new List<Tund>();

        public List<List<Tund>> nadal = new List<List<Tund>>();

        public List<Tund> Esmaspaev {
            get
            {
                return esmaspaev;
            }
            set
            {
                esmaspaev = value;
                nadal.Add(esmaspaev);
            }
        }
        public List<Tund> Teisipaev
        {
            get
            {
                return teisipaev;
            }
            set
            {
                teisipaev = value;
                nadal.Add(teisipaev);
            }
        }
        public List<Tund> Kolmapaev
        {
            get
            {
                return kolmapaev;
            }
            set
            {
                kolmapaev = value;
                nadal.Add(kolmapaev);
            }
        }
        public List<Tund> Neljapaev
        {
            get
            {
                return neljapaev;
            }
            set
            {
                neljapaev = value;
                nadal.Add(neljapaev);
            }
        }
        public List<Tund> Reede {
            get
            {
                return reede;
            }
            set
            {
                reede = value;
                nadal.Add(reede);
            }
        }
    }

    public class Tunniplaan
    {
        public string nadal { get; set; }
        public Ajad ajad { get; set; }
        public Tunnid tunnid { get; set; }
        public string viimane_muudatus { get; set; }
    }
}
