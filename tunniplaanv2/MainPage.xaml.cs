﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using System.Net.Http;
using Newtonsoft.Json;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace tunniplaanv2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            this.getGruppTunniplaan(new DateTime(), 213);
        }

        private async void getGruppTunniplaan(DateTime date, int gruppID)
        {
            String sDate = date.ToString("yyyy-mm-dd");
            String gruppid = gruppID.ToString();
            var http = new HttpClient();
            HttpResponseMessage response;
            try
            {
                response = await http.GetAsync("https://vikk.siseveeb.ee/veebilehe_andmed/tunniplaan/?nadal=2018-02-26&grupp=213");
            }
            catch (System.Net.Http.HttpRequestException)
            {
                getGruppTunniplaan(date, gruppID); //recurse that shit until it werks
            }
            var result = await response.Content.ReadAsStringAsync();
            Tunniplaan tunniplaan = JsonConvert.DeserializeObject<Tunniplaan>(result);



            List<ListView> dayUIElement = new List<ListView>()
            {
                Esmaspaev, Teisipaev, Kolmapaev, Neljapaev, Reede
            };

            for( int iDay = 0; iDay < 5; iDay++)
            {
                var paev = tunniplaan.tunnid.nadal[iDay];

                    foreach(var tund in paev)
                    {

                        bool foundLesson = false;

                        for (int i = 0; i < 9; i++)
                        {
                            if (Convert.ToInt32(tund.tund) == i)
                            {
                                ListViewItem tempItem = new ListViewItem();
                                tempItem.Content = tund.tund + tund.aine + tund.algus + "-" + tund.lopp;
                                dayUIElement[iDay].Items.Add(tempItem);
                                foundLesson = true;
                                break;
                            }
                        }

                        if (foundLesson == false)
                        {
                            ListViewItem jewishItem = new ListViewItem();
                            jewishItem.Content = "";
                            dayUIElement[iDay].Items.Add(jewishItem);
                        }
                    }
            }

            

            //Esmaspaev.Items.Add()
        }



    }
}
